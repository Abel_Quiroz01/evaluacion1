/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.modelo;

// Autor: Abel Quiroz

public class CalculadoraInteres {
    
    private double Capital;
    private double Tasa_Anual;
    private double numeroAlms;
    private double resultado;

    // GETTERS Y SETTERS
    
    public double getCapital() {
        return Capital;
    }

    public void setCapital(double capital) {
        this.Capital = capital;
    }

    public double getTasa_Anual() {
        return Tasa_Anual;
    }
    
    public void setTasa_Anual(double tasaAnual) {
        this.Tasa_Anual = tasaAnual;
    }

    public double getnumeroAlms() {
        return numeroAlms;
    }

    public void setnumeroAlms(double numAnos) {
        this.numeroAlms = numAnos;
    }
    
    public double getResultado() {
        return resultado;
    }

    public void setResultado(double resultado) {
        this.resultado = resultado;
    }
    
    public String calcular(){
        String resultadoCalculo;
        System.out.println("Entrando a la 2da funcion calcular");
        System.out.println("Capital: " + this.getCapital());
        System.out.println("Tasa Anual: " + this.getTasa_Anual());
        System.out.println("Número de años: " + this.getnumeroAlms());
        System.out.println("Resultado: " + (this.getCapital() * (this.getTasa_Anual()/100) * this.getnumeroAlms()));
        resultadoCalculo = Double.toString(this.getCapital() * (this.getTasa_Anual()/100) * this.getnumeroAlms());
        return "$" + resultadoCalculo.replace(".", ",");

    }
    
    public String calcular(double capital, double tasaAnual, double numAnos){       
        System.out.println("Entrando a la funcion calcular");      
        this.setCapital(capital);
        this.setTasa_Anual(tasaAnual);
        this.setnumeroAlms(numAnos);
        return calcular();
    }
    
}
