<%-- 
    Document   : resultado
    Created on : Apr 11, 2021, 4:00:58 PM
    Author     : Abel
--%>

<%@page import="root.modelo.CalculadoraInteres"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    CalculadoraInteres cal = (CalculadoraInteres)request.getAttribute("calculadora");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col" id="divPrincipal">
                    <h4 style="text-align: center;">El resultado del interes calculado es:</h4>
                    <form name="form" action="CalculadoraInteresController" method="POST">
                        <h1 name="resultado" style="text-align: center;"><%= cal.calcular()%></h1>
                    </form>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </body>
</html>

