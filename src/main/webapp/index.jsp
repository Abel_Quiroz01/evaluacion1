<%-- 
    Document   : index
    Created on : Apr 11, 2021, 4:00:40 PM
    Author     : Abel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col"></div>
                <div class="col" id="divPrincipal">
                    <h1>UNI01 - Calculator</h1>
                    <hr>
                        <form name="Formulario" action="CalculadoraInteresController" method="POST">
                            <div class="form-group row">
                                <label for="numCapital" class="col-sm-4 col-form-label">Ingrese el Capital:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="numCapital" class="form-control" placeholder="Capital" required></input>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="numTasa" class="col-sm-4 col-form-label">Ingrese la Tasa Anual:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="numTasa" class="form-control" placeholder="Tasa Anual" required></input>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="numYear" class="col-sm-4 col-form-label">Número de Años:</label>
                                <div class="col-sm-8">
                                    <input type="number" name="numYear" class="form-control" placeholder="Número de Años" required></input>
                                </div>
                            </div>
                            <hr>
                            <button type="submit" name="calcular" value="miBoton" class="btn btn-primary btn-block">Calcular</button>
                        </form>
                </div>
                <div class="col"></div>
            </div>
        </div>
    </body>
</html>
